import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

public class ExecuteHibernate {
    private static SessionFactory factory;
    public static void main(String[] args){

        try {
            factory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Failed to create sessionFactory object." + ex);
            throw new ExceptionInInitializerError(ex);
        }

        ExecuteHibernate executeHibernate = new ExecuteHibernate();

        // Adding customer info to customer table...
        Integer customerID1 = executeHibernate.addCustomer("Daniel","De Jesus" ,"Guzman", "424-324-9963","guz16005@byui.edu" ,216578900);
        Integer customerID2 = executeHibernate.addCustomer("Shary", " ", " Guzman", "208-468-3444", "sharyromano@gmail.com", 216578901);
        Integer customerID3 = executeHibernate.addCustomer("Alice", " Rose", "Wixom", "729-643-2210", "Arose23@yahoo.com", 216578902);
        Integer customerID4 = executeHibernate.addCustomer("James", " " , " Johnson", "895-978-4134", " Jjhonson41@hotmail.com", 216578903);
        Integer customerID5 = executeHibernate.addCustomer("Mariela" ,"De la Luz", "Concepcion", "323-894-5391", " ", 216578904);


        // Adding bank info to bank table...
        Integer bankID1 = executeHibernate.addBank("Chase", 801401, "310-458-1468", "1333 4th St, Santa Monica, CA 90401", "store1333@chase.com");
        Integer bankID2 = executeHibernate.addBank("Bank of America", 746901, "310-434-2260", "1430 Wilshire Blvd, Santa Monica, CA 90403", "1430Wilshire@bankAmerica.com");
        Integer bankID3 = executeHibernate.addBank("Well Fargo", 531731, "310-450-0749", "2940 Ocean Park Blvd, Santa Monica, CA 90405", "wellsFargo@bank.com");
        Integer bankID4 = executeHibernate.addBank("Comerica Bank",974345, "800-522-2265", "4040 Lincoln Blvd, Marina Del Rey, CA 90292", " " );
        Integer bankID5 = executeHibernate.addBank("OneWest Bank", 341000, "310-577-6142", "13405 Washington Blvd, Marina Del Rey, CA 90292", "onewestbank.com");

    }

    /* Method to CREATE a customer in the database */
    public Integer addCustomer(String fname, String middle_name, String lname, String phone, String email, int customer_id){
        Session session = factory.openSession();
        Transaction tx = null;
        Integer customerID = null;

        try {
            tx = session.beginTransaction();
            Customer customer = new Customer(fname, middle_name, lname, phone, email, customer_id );
            customerID = (Integer) session.save(customer);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return customerID;
    }

    /* Method to CREATE a customer in the database */
    public Integer addBank(String bank_name, int bank_id, String bank_phone_numb, String b_address, String b_email){
        Session session2 = factory.openSession();
        Transaction tx = null;
        Integer bankID = null;

        try {
            tx = session2.beginTransaction();
            Bank bank = new Bank(bank_name, bank_id, bank_phone_numb, b_address, b_email);
            bankID = (Integer) session2.save(bank);
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session2.close();
        }
        return bankID;
    }

}
