public class Bank {

    private int id;
    private String bank_name;
    private int bank_id;
    private String bank_phone_numb;
    private String b_address;
    private String b_email;

    public Bank(){

    }

    public Bank( String bank_name, int bank_id, String bank_phone_numb, String b_address, String b_email) {
        this.bank_name = bank_name;
        this.bank_id = bank_id;
        this.bank_phone_numb = bank_phone_numb;
        this.b_address = b_address;
        this.b_email = b_email;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public int getBank_id() {
        return bank_id;
    }

    public void setBank_id(int bank_id) {
        this.bank_id = bank_id;
    }

    public String getBank_phone_numb() {
        return bank_phone_numb;
    }

    public void setBank_phone_numb(String bank_phone_numb) {
        this.bank_phone_numb = bank_phone_numb;
    }

    public String getB_address() {
        return b_address;
    }

    public void setB_address(String b_address) {
        this.b_address = b_address;
    }

    public String getB_email() {
        return b_email;
    }

    public void setB_email(String b_email) {
        this.b_email = b_email;
    }
}
